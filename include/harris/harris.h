//
// Created by Komorowicz David on 2020. 06. 08..
//

#pragma once

class Harris {
public:
    Harris(unsigned * image, int width, int height);
    int getNumCores();
};

